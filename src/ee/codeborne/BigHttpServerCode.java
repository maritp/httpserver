package ee.codeborne;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.ServerSocket;
import java.net.Socket;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Date;

import static java.nio.charset.StandardCharsets.UTF_8;


public class BigHttpServerCode {
    public static void main(String[] args) throws IOException {
        ServerSocket server = new ServerSocket(8088);
        System.out.println("Waiting for a knock!");
        while (true) {
            initiatingConnection(server);
        }
    }

    private static void initiatingConnection(ServerSocket server) {
        try (Socket client = server.accept(); BufferedReader reader = new BufferedReader(new InputStreamReader(client.getInputStream()))) {
            System.out.println("Knock knock");

            String firstLine = getRequestLine(reader);
            if (firstLine == null && !"GET".equals(firstLine.split(" ")[0]) && !"HEAD".equals(firstLine.split(" ")[0])) {
                createResponse(client, null, null);
                return;
            }
            if ("/".equals(firstLine.split(" ")[1])) {
                String status = "200" + firstLine.split(" ")[0];
                createResponse(client, status, null);
                return;
            }
            String pathName = firstLine.split(" ")[1].replace("/", "");
            Path path = Paths.get("src", pathName);
            if (path.toFile().exists()) {
                String request = firstLine.split(" ")[0];
                readingPath(client, path, request);
            } else {
                String status = "404";
                byte[] body = (pathName + " file not found!").getBytes(UTF_8);
                createResponse(client, status, body);
            }
        } catch (IOException ex) {
            ex.printStackTrace();
        }
    }

    private static String getRequestLine(BufferedReader reader) throws IOException {
        String line = reader.readLine();
        if (line != null) {
            return line;
        }
        return null;
    }

    private static void readingPath(Socket client, Path pathName, String request) {
        try {
            String status = "200 " + request + ";" + pathName;
            System.out.println(status);
            byte[] data = Files.readAllBytes(pathName);
            createResponse(client, status, data);
        } catch (IOException ex) {
            ex.printStackTrace();
        }
    }

    private static void createResponse(Socket client, String status, byte[] bodyInput) throws IOException {
        if (status == null) {
            String statusLine = "400 Bad Request";
            String bodyString = "Bad RequestLine!";
            byte[] body = bodyString.getBytes(UTF_8);
            sendResponse(client, statusLine, body);
            return;
        }
        if ("200".equals(status.substring(0, 3))) {
            if (bodyInput != null) {
                String test = status.split(";")[1];
                System.out.println(test);
                Path path = Paths.get(status.split(";")[1]);
                String statusLine = "200 OK\n";
                String header = "Date: " + new Date();
                String statusInput = statusLine + header + "\n\n";
                if ("GET".equals(status.split(" ")[1].split(";")[0])) {
                    String headerReplacement = header + "\nContent-Length: " + bodyInput.length +
                            "\nContent-Type: " + Files.probeContentType(path);
                    statusInput = statusLine + headerReplacement + "\n\n";
                    sendResponse(client, statusInput, bodyInput);
                    return;
                }
                sendResponse(client, statusInput, null);
                return;
            } else {
                String statusLine = "200 OK\n";
                String header = "Date: " + new Date();
                String statusInput = statusLine + header + "\n\n";
                if ("GET".equals(status.substring(3))) {
                    Date today = new Date();
                    byte[] body = today.toString().getBytes(UTF_8);
                    String headerReplacement = header + "\nContent-Length: " + body.length;
                    statusInput = statusLine + headerReplacement + "\n\n";
                    sendResponse(client, statusInput, body);
                } else {
                    sendResponse(client, statusInput, null);
                }
            }
            return;
        }
        if ("404".equals(status)) {
            String statusLine = "404 not found \n\n";
            sendResponse(client, statusLine, bodyInput);
        }
    }

    private static void sendResponse(Socket client, String input, byte[] body) {
        String protocol = "HTTP/1.1 ";
        String serverInput = protocol + input;
        try {
            client.getOutputStream().write(serverInput.getBytes(UTF_8));
            if (body != null) {
                client.getOutputStream().write(body);
            }
        } catch (IOException ex) {
            System.out.println("Error in responding to the client");
            ex.printStackTrace();
        }
    }
}
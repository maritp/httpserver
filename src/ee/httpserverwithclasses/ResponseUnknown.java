package ee.httpserverwithclasses;

import java.util.Map;
import java.util.TreeMap;

import static ee.httpserverwithclasses.ServerStatus.BAD_REQUEST;

public class ResponseUnknown extends Response {
    ResponseUnknown() {
        super();
        serverStatus = BAD_REQUEST;
    }

    @Override
    public Map<String, Object> getHeaders() {
        return new TreeMap<>();
    }

    @Override
    public byte[] getBody() {
        return new byte[0];
    }
}

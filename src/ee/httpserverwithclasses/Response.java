package ee.httpserverwithclasses;

import java.io.IOException;
import java.util.Map;

import static java.nio.charset.StandardCharsets.UTF_8;

abstract class Response {
    ServerStatus serverStatus;

    public abstract Map<String, Object> getHeaders();

    public abstract byte[] getBody() throws IOException;

    String compileStatusLineAndHeaders() {
        String statusLine = "HTTP/1.1 " + serverStatus.getMessage();
        String headersOverall = "";

        for (Map.Entry<String, Object> entry : getHeaders().entrySet()) {
            headersOverall = headersOverall + "\n" + entry.getKey() + ": " + entry.getValue();
        }
        String headers = headersOverall + "\n\n";

        return statusLine + headers;
    }

    byte[] getResponse() throws IOException {
        byte[] statusLineAndHeaders = compileStatusLineAndHeaders().getBytes(UTF_8);
        byte[] all = new byte[statusLineAndHeaders.length + getBody().length];

        System.arraycopy(statusLineAndHeaders, 0, all, 0, statusLineAndHeaders.length);
        System.arraycopy(getBody(), 0, all, statusLineAndHeaders.length, getBody().length);

        return all;
    }
}
package ee.httpserverwithclasses;

public enum ServerStatus {
    OK("200 OK"), BAD_REQUEST("400 Bad Request"), NOT_FOUND("404 Not Found");

    private String message;

    ServerStatus(String message) {
        this.message = message;
    }

    public String getMessage() {
        return this.message;
    }
}

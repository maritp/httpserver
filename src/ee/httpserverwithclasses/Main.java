package ee.httpserverwithclasses;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.ArrayList;
import java.util.List;

public class Main {
    public static void main(String[] args) throws IOException {
        ServerSocket server = new ServerSocket(8888);

        Main main = new Main();
        main.clientInputAndResponse(server);
    }

    private void clientInputAndResponse(ServerSocket server) {
        while (true) {
            try (Socket client = server.accept()) {
                oneRequest(client);
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    void oneRequest(Socket client) {
        try (OutputStream responseOutput = client.getOutputStream(); BufferedReader clientInput = parseClientInputIntoBufferedReader(client)) {

            handle(clientInput, responseOutput);

        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    BufferedReader parseClientInputIntoBufferedReader(Socket client) throws IOException {
        return new BufferedReader(new InputStreamReader(client.getInputStream()));
    }

    void handle(BufferedReader clientInput, OutputStream responseOutput) throws IOException {
        Request request = initializeRequestWithInput(clientInput);
        if (request.inputList.size() > 0) {
            Response response = methodControl(request);
            responseOutput.write(response.getResponse());
        }
    }

    Request initializeRequestWithInput(BufferedReader clientInput) throws IOException {
        List<String> inputList = new ArrayList<>();
        String line = clientInput.readLine();

        while (!"".equals(line) && line != null) {
            inputList.add(line);
            line = clientInput.readLine();
        }

        int bodyLengthInt = getContentLength(inputList);

        char[] bodyCharArray = getInputBody(clientInput, bodyLengthInt);

        return new Request().parseRequest(inputList, bodyCharArray);
    }

    int getContentLength(List<String> inputList) {
        int bodyLengthInt = 0;
        for (String inputRow : inputList) {
            if ("Content-Length".equals(inputRow.split(":")[0].trim())) {
                bodyLengthInt = Integer.parseInt(inputRow.split(":", 2)[1].trim());
            }
        }
        return bodyLengthInt;
    }

    char[] getInputBody(BufferedReader clientInput, int bodyLengthInt) throws IOException {
        char[] bodyCharArray = new char[bodyLengthInt];
        int x = 0;
        while (x < bodyLengthInt) {
            int charsRead = clientInput.read(bodyCharArray, x, bodyLengthInt - x);
            if (charsRead > -1) {
                x += charsRead;
            } else {
                break;
            }
        }
        return bodyCharArray;
    }

    Response methodControl(Request request) {
        Response response;
        if ("GET".equals(request.getMethod())) {
            response = new ResponseGET(request);
        } else if ("HEAD".equals(request.getMethod())) {
            response = new ResponseHEAD(request);
        } else {
            response = new ResponseUnknown();
        }
        return response;
    }
}
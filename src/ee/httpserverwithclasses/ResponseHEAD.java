package ee.httpserverwithclasses;

import java.io.IOException;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Map;
import java.util.TreeMap;

import static ee.httpserverwithclasses.ServerStatus.NOT_FOUND;
import static ee.httpserverwithclasses.ServerStatus.OK;
import static java.nio.charset.StandardCharsets.UTF_8;

public class ResponseHEAD extends Response {
    byte[] defaultBody = new Date().toString().getBytes(UTF_8);
    Request request;
    Path path;

    ResponseHEAD(Request request){
        super();
        getServerStatus(request);
    }

    void getServerStatus(Request request) {
        this.request = request;
        path = getPath(request);

        if (!path.toFile().exists()) {
            serverStatus = NOT_FOUND;
            return;
        }
        serverStatus = OK;
    }

   Path getPath(Request request) {
        return Paths.get("src", request.getUri().replace("/",""));
    }

    @Override
    public Map<String, Object> getHeaders() {
        Map<String, Object> headers = new TreeMap<>();

        if (!path.toFile().exists()) {
            return headers;
        }

        headers.put("Date", compileDate());

        if (!"/".equals(request.getUri())) {
            headers.put("Content-Length", path.toFile().length());
        } else {
            headers.put("Content-Length", defaultBody.length);
        }
        return headers;
    }

    String compileDate() {
        return new SimpleDateFormat("yyyy-MM-dd").format(now());
    }

    Date now() {
        return new Date();
    }

    @Override
    public byte[] getBody() throws IOException {
        return new byte[0];
    }
}
package ee.httpserverwithclasses;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;

public class ResponseGET extends ResponseHEAD {

    ResponseGET(Request request) {
        super(request);
    }

    @Override
    public byte[] getBody() throws IOException {
        if (hasDefaultBody()) return defaultBody;

        if (hasFileBody()) return getFileContent();

        return new byte[0];
    }

    boolean hasDefaultBody() {
        return "/".equals(request.getUri());
    }

    boolean hasFileBody() {
        File file = path.toFile();
        return file.exists();
    }

    byte[] getFileContent() throws IOException {
        return Files.readAllBytes(path);
    }
}

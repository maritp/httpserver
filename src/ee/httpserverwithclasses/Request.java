package ee.httpserverwithclasses;

import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

class Request {
    List<String> inputList = new ArrayList<>();
    private Map<String, String> requestHeadersMap = new LinkedHashMap<>();
    private char[] bodyContent;
    private String method;
    private String uri;

    Request parseRequest(List<String> inputList, char[] inputBody) {
        this.inputList = inputList;
        this.bodyContent = inputBody;
        parseRequestLine();
        addRequestHeadersToMap();
        return this;
    }

    void parseRequestLine() {
        if (inputList.size() > 0) {
            String[] split = inputList.get(0).split(" ");
            if (split.length > 1) {
                method = split[0];
                uri = split[1];
            }
        }
    }

    void addRequestHeadersToMap() {
        for (String inputRow : inputList) {
            if (inputRow.split(":").length > 1) {
                String[] splitArrayList = inputRow.split(":", 2);
                requestHeadersMap.put(splitArrayList[0].trim(), splitArrayList[1].trim());
            }
        }
    }

    Map<String, String> getRequestHeadersMap() {
        return requestHeadersMap;
    }

    char[] getBodyContent() {
        return bodyContent;
    }

    String getMethod() {
        return method;
    }

    String getUri() {
        return uri;
    }
}
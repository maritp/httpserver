package ee.httpserverwithclasses;

import org.junit.Test;

import java.util.LinkedHashMap;
import java.util.Map;

import static java.nio.charset.StandardCharsets.UTF_8;
import static org.junit.Assert.assertArrayEquals;
import static org.junit.Assert.assertEquals;

public class ResponseTest {

    private Response response = new Response() {

        {
            serverStatus = ServerStatus.OK;
        }

        @Override
        public Map<String, Object> getHeaders() {
            Map<String, Object> weAreHeaders = new LinkedHashMap<>();
            weAreHeaders.put("Here is", "1 header");
            weAreHeaders.put("Adding more headers", 2);

            return weAreHeaders;
        }

        @Override
        public byte[] getBody() {
            return ("I am A magnificent body").getBytes(UTF_8);
        }
    };

    @Test
    public void getStatusLineAndHeaders() {
        assertEquals("HTTP/1.1 200 OK\nHere is: 1 header\nAdding more headers: 2\n\n", response.compileStatusLineAndHeaders());
    }

    @Test
    public void getResponse() throws Exception {
        assertArrayEquals("HTTP/1.1 200 OK\nHere is: 1 header\nAdding more headers: 2\n\nI am A magnificent body".getBytes(UTF_8), response.getResponse());
    }
}
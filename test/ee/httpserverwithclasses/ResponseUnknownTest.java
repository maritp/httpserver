package ee.httpserverwithclasses;

import org.junit.Test;

import java.io.IOException;
import java.util.Map;
import java.util.TreeMap;

import static org.junit.Assert.assertArrayEquals;
import static org.junit.Assert.assertEquals;

public class ResponseUnknownTest {

    private Response response = new ResponseUnknown();

    @Test
    public void ifMethodIsUnknown() throws IOException {
        assertArrayEquals(("HTTP/1.1 400 Bad Request\n\n").getBytes(), response.getResponse());
    }

    @Test
    public void getHeaders() {
        Map<String, Object> tree = new TreeMap<>();
        assertEquals(tree, response.getHeaders());
    }

    @Test
    public void getBody() throws IOException {
        assertArrayEquals(new byte[0], response.getBody());
    }
}
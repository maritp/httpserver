package ee.httpserverwithclasses;

import org.junit.Test;

import java.util.List;

import static java.util.Arrays.asList;
import static java.util.Collections.singletonList;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNull;
import static org.mockito.Mockito.spy;

public class RequestTest {

    @Test
    public void parseRequest_ifRequestLineIsOneWord() {
        char[] emptyCharArray = new char[0];
        List<String> testInputList = singletonList("GOOOOD!");
        Request request = new Request();

        request.parseRequest(testInputList, emptyCharArray);

        assertNull(request.getMethod());
    }

    @Test
    public void parseRequestLine() {
        Request request = new Request();
        request.inputList = asList("HEAD /fun.jpg protocol", "I'm the fist header : number 1");

        request.parseRequestLine();

        assertEquals("HEAD", request.getMethod());
        assertEquals("/fun.jpg", request.getUri());
    }

    @Test
    public void addRequestHeadersToMap() {
        Request requestSpy = spy(Request.class);

        requestSpy.inputList = asList("This is a requestLine","Head : 1", "Header : 2"," Header num : 3", "Header number : 4 addition", "Header header : 5");

        requestSpy.addRequestHeadersToMap();

        assertEquals(5, requestSpy.getRequestHeadersMap().size());
        assertEquals("1", requestSpy.getRequestHeadersMap().get("Head"));
        assertEquals("4 addition", requestSpy.getRequestHeadersMap().get("Header number"));
    }

    @Test
    public void addRequestHeadersToMap_withExtraSpaces() {
        Request requestSpy = spy(Request.class);

        requestSpy.inputList = asList("This is a requestLine","    Head : 1      ", "Header : 2"," Header num : 3", "             Header number : 4 addition", "Header header : 5          ");

        requestSpy.addRequestHeadersToMap();

        assertEquals(5, requestSpy.getRequestHeadersMap().size());
        assertEquals("1", requestSpy.getRequestHeadersMap().get("Head"));
        assertEquals("4 addition", requestSpy.getRequestHeadersMap().get("Header number"));
        assertEquals("5", requestSpy.getRequestHeadersMap().get("Header header"));
    }

    @Test
    public void addRequestHeadersToMap_withManySemicolons() {
        Request requestSpy = spy(Request.class);

        requestSpy.inputList = asList("This is a requestLine","Head : 1", "Header : 2"," Header num : 3::Hi", "Header number : 4 : addition", "Header header : 5");

        requestSpy.addRequestHeadersToMap();

        assertEquals(5, requestSpy.getRequestHeadersMap().size());
        assertEquals("3::Hi", requestSpy.getRequestHeadersMap().get("Header num"));
        assertEquals("4 : addition", requestSpy.getRequestHeadersMap().get("Header number"));
    }

    @Test
    public void getBodyContent() {
        Request request = new Request();
        char[] testChar = new char["I'm a body".charAt(0)];
        List<String> testInputList = singletonList("The");

        request.parseRequest(testInputList, testChar);

        assertEquals(testChar, request.getBodyContent());
    }

    @Test
    public void getMethod() {
        Request request = new Request();
        char[] testCharArray = new char[0];
        List<String> testInputList = singletonList("GET url");

        Request result = request.parseRequest(testInputList, testCharArray);

        assertEquals("GET", result.getMethod());
    }

    @Test
    public void getURI()  {
        Request request = new Request();
        char[] testCharArray = new char[0];
        List<String> testInputList = singletonList("GET /test.txt");

        Request result = request.parseRequest(testInputList,testCharArray);

        assertEquals("/test.txt", result.getUri());
    }
}
package ee.httpserverwithclasses;

import org.junit.Test;
import org.mockito.Answers;

import java.io.File;
import java.io.IOException;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.List;
import java.util.Map;

import static ee.httpserverwithclasses.ServerStatus.NOT_FOUND;
import static ee.httpserverwithclasses.ServerStatus.OK;
import static java.nio.charset.StandardCharsets.UTF_8;
import static java.util.Collections.singletonList;
import static org.junit.Assert.*;
import static org.mockito.Mockito.*;

public class ResponseGETTest {

    @Test
    public void getBody() throws Exception {
        ResponseGET response = mock(ResponseGET.class);

        when(response.getBody()).thenCallRealMethod();
        when(response.hasDefaultBody()).thenReturn(false);
        when(response.hasFileBody()).thenReturn(true);
        when(response.getFileContent()).thenReturn("hi".getBytes(UTF_8));

        assertArrayEquals("hi".getBytes(), response.getBody());
    }

    @Test
    public void getDefaultBody() {
        ResponseGET response = mock(ResponseGET.class);
        response.request = mock(Request.class);

        when(response.request.getUri()).thenReturn("/");
        when(response.hasDefaultBody()).thenCallRealMethod();

        assertTrue(response.hasDefaultBody());
    }

    @Test
    public void hasFileBody() {
        ResponseGET response = mock(ResponseGET.class);

        response.path = mock(Path.class);
        File file = mock(File.class);
        when(response.path.toFile()).thenReturn(file);
        when(file.exists()).thenReturn(true);
        when(response.hasFileBody()).thenCallRealMethod();

        assertTrue(response.hasFileBody());
    }

    @Test
    public void testPath() throws Exception {
        ResponseGET response = mock(ResponseGET.class);
        response.path = Paths.get("test", "RGET.txt");

        when(response.getFileContent()).thenCallRealMethod();

        assertArrayEquals("Tere testime testi!".getBytes(), response.getFileContent());
    }

    @Test
    public void getStatusLineAndHeaders() {
        ResponseGET response = mock(ResponseGET.class);
        response.request = mock(Request.class);
        response.path = Paths.get("test", "RGET.txt");

        when(response.request.getUri()).thenReturn("/hi");
        when(response.compileDate()).thenReturn("I am a date");
        when(response.getHeaders()).thenCallRealMethod();

        Map<String, Object> headers = response.getHeaders();

        assertEquals(2, headers.size());
        assertEquals("I am a date", headers.get("Date"));
        assertEquals(19L, headers.get("Content-Length"));
    }

    @Test
    public void getResponseWhenFileDoesNotExist() throws IOException {
        List<String> firstLineList = singletonList("GET file");
        char[] testCharArray = new char[0];
        Request request = new Request();
        request.parseRequest(firstLineList, testCharArray);
        ResponseGET response = new ResponseGET(request);
        response.path = Paths.get("test", "nofile.txt");

        byte[] result = response.getResponse();

        assertArrayEquals(("HTTP/1.1 404 Not Found\n\n").getBytes(UTF_8), result);
    }

    @Test
    public void getServerStatusIfFileDoesNotExist() {
        ResponseGET response = mock(ResponseGET.class, Answers.CALLS_REAL_METHODS);
        Request request = mock(Request.class);
        Path path = mock(Path.class);
        File file = mock(File.class);

        doReturn(path).when(response).getPath(request);
        when(path.toFile()).thenReturn(file);
        when(file.exists()).thenReturn(false);

        response.getServerStatus(request);

        assertEquals(NOT_FOUND, response.serverStatus);
    }

    @Test
    public void getServerStatusIfFileExists() {
        ResponseGET response = mock(ResponseGET.class, Answers.CALLS_REAL_METHODS);
        Request request = mock(Request.class);
        Path path = mock(Path.class);
        File file = mock(File.class);

        doReturn(path).when(response).getPath(request);
        when(path.toFile()).thenReturn(file);
        when(file.exists()).thenReturn(true);

        response.getServerStatus(request);

        assertEquals(OK, response.serverStatus);
    }
}
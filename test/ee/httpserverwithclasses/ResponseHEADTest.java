package ee.httpserverwithclasses;

import org.junit.Test;
import org.mockito.Answers;

import java.io.File;
import java.nio.file.Path;
import java.text.SimpleDateFormat;
import java.util.Map;

import static ee.httpserverwithclasses.ServerStatus.NOT_FOUND;
import static ee.httpserverwithclasses.ServerStatus.OK;
import static java.nio.charset.StandardCharsets.UTF_8;
import static org.junit.Assert.*;
import static org.mockito.Mockito.*;

public class ResponseHEADTest {

    @Test
    public void getServerStatus_IfFileDoesNotExist() {
        ResponseHEAD response = mock(ResponseHEAD.class, Answers.CALLS_REAL_METHODS);
        Request request = mock(Request.class);
        Path path = mock(Path.class);
        File file = mock(File.class);

        doReturn(path).when(response).getPath(request);
        when(path.toFile()).thenReturn(file);
        when(file.exists()).thenReturn(false);

        response.getServerStatus(request);

        assertEquals(NOT_FOUND, response.serverStatus);
    }

    @Test
    public void getServerStatus_IfFileExists() {
        ResponseHEAD response = mock(ResponseHEAD.class, Answers.CALLS_REAL_METHODS);
        Request request = mock(Request.class);
        Path path = mock(Path.class);
        File file = mock(File.class);

        doReturn(path).when(response).getPath(request);
        when(path.toFile()).thenReturn(file);
        when(file.exists()).thenReturn(true);

        response.getServerStatus(request);

        assertEquals(OK, response.serverStatus);
    }

    @Test
    public void getHeaders_WhenFileDoesNotExist(){
        ResponseHEAD response = mock(ResponseHEAD.class);
        response.path = mock(Path.class);
        File file = mock(File.class);

        when(response.path.toFile()).thenReturn(file);
        when(file.exists()).thenReturn(false);
        when(response.getHeaders()).thenCallRealMethod();

        assertTrue(response.getHeaders().isEmpty());
    }

    @Test
    public void getHeaders_ForDefaultPage() {
        ResponseHEAD response = mock(ResponseHEAD.class);
        response.request = mock(Request.class);
        response.path = mock(Path.class);
        File file = mock(File.class);

        when(response.path.toFile()).thenReturn(file);
        when(file.exists()).thenReturn(true);
        when(response.compileDate()).thenReturn("DATE");
        when(response.request.getUri()).thenReturn("/f");
        when(file.length()).thenReturn(5L);
        when(response.getHeaders()).thenCallRealMethod();

        Map<String, Object> headers = response.getHeaders();

        assertEquals(2, headers.size());
        assertEquals(5L, headers.get("Content-Length"));
        assertEquals("DATE", headers.get("Date"));
    }

    @Test
    public void getHeaders_ForFile() {
        ResponseHEAD response = mock(ResponseHEAD.class);
        response.request = mock(Request.class);
        response.path = mock(Path.class);
        response.defaultBody = "AAB".getBytes(UTF_8);

        File file = mock(File.class);

        when(response.path.toFile()).thenReturn(file);
        when(file.exists()).thenReturn(true);
        when(response.compileDate()).thenReturn("DATE");
        when(response.request.getUri()).thenReturn("/");
        when(response.getHeaders()).thenCallRealMethod();

        Map<String, Object> headers = response.getHeaders();

        assertEquals(2, headers.size());
        assertEquals(3, headers.get("Content-Length"));
        assertEquals("DATE", headers.get("Date"));
    }

    @Test
    public void getBody() throws Exception {
        ResponseHEAD response = mock(ResponseHEAD.class);

        when(response.getBody()).thenCallRealMethod();

        assertArrayEquals(new byte[0], response.getBody());
    }

    @Test
    public void compileDate() throws Exception {
        ResponseHEAD response = mock(ResponseHEAD.class);

        when(response.compileDate()).thenCallRealMethod();
        when(response.now()).thenReturn(new SimpleDateFormat("dd.MM-yyyy").parse("08.10-2010"));

        String result = response.compileDate();
        assertEquals("2010-10-08", result);
    }
}
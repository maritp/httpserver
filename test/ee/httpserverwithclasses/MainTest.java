package ee.httpserverwithclasses;

import org.junit.Test;

import java.io.*;
import java.net.Socket;
import java.util.Collections;
import java.util.List;

import static java.util.Arrays.asList;
import static org.junit.Assert.*;
import static org.mockito.Mockito.*;

public class MainTest {


    @Test
    public void oneRequest() throws IOException {
        Main main = spy(Main.class);
        Socket testSocket = mock(Socket.class);
        OutputStream testOutputStream = mock(OutputStream.class);
        BufferedReader testBufferedReader = mock(BufferedReader.class);

        doReturn(testOutputStream).when(testSocket).getOutputStream();
        doReturn(testBufferedReader).when(main).parseClientInputIntoBufferedReader(testSocket);

        main.oneRequest(testSocket);

        verify(main).handle(testBufferedReader, testOutputStream);
    }

    @Test
    public void handle_checkIfRequestGetsAnswer() throws IOException {
        Main main = spy(Main.class);
        Response responseMock = mock(Response.class);
        Request request = spy(Request.class);
        request.inputList = Collections.singletonList("Hello");
        BufferedReader bufferedReader = mock(BufferedReader.class);
        ByteArrayOutputStream byteArrayOutputStream = mock(ByteArrayOutputStream.class);

        doReturn(responseMock).when(main).methodControl(any());
        doReturn("123".getBytes()).when(responseMock).getResponse();
        doReturn(request).when(main).initializeRequestWithInput(bufferedReader);

        main.handle(bufferedReader, byteArrayOutputStream);

        verify(main).initializeRequestWithInput(bufferedReader);
        verify(byteArrayOutputStream).write("123".getBytes());
    }

    @Test
    public void initializeRequestWithInput_getInputAsList() throws IOException {
        ByteArrayInputStream inputStream = new ByteArrayInputStream("Greetings\nHow Is Life\nGood?".getBytes());
        BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(inputStream));

        Request result = new Main().initializeRequestWithInput(bufferedReader);

        assertEquals(asList("Greetings", "How Is Life", "Good?"), result.inputList);
    }

    @Test
    public void initializeRequestWithInput_withEmptyInput() throws IOException {
        BufferedReader bufferedReader = mock(BufferedReader.class);
        when(bufferedReader.readLine()).thenReturn(null);

        Request result = new Main().initializeRequestWithInput(bufferedReader);

        assertTrue(result.inputList.isEmpty());
    }

    @Test
    public void initializeRequestWithInput_WithBody() throws IOException {
        String body = "Body";
        ByteArrayInputStream inputStream = new ByteArrayInputStream(("RequestMethod Uri Protocol\nHeader: 1\nContent-Length: " + body.length() + "\n\n" + body).getBytes());
        BufferedReader bufferedReader = new BufferedReader((new InputStreamReader(inputStream)));

        Request result = new Main().initializeRequestWithInput(bufferedReader);

        assertEquals(4, result.getBodyContent().length);
    }

    @Test
    public void getContentLength() {
        Main main = spy(Main.class);
        List<String> listOfHeaders = asList("RequestMethod Uri Protocol","Header: 1","Content-Length: 15","Other:header");

        int result = main.getContentLength(listOfHeaders);

        assertEquals(15, result);
    }

    @Test
    public void getBodyInput() throws IOException {
        Main main = spy(Main.class);

        ByteArrayInputStream inputStream = new ByteArrayInputStream("abcdefg".getBytes());
        BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(inputStream));

        char[] resultCharArray = main.getInputBody(bufferedReader, 7);

        char[] testCharArray = "abcdefg".toCharArray();

        assertArrayEquals(testCharArray, resultCharArray);
    }

    @Test
    public void getBodyInput_whenContentLengthIsSmallerThanBody() throws IOException {
        Main main = spy(Main.class);
        BufferedReader bufferedReader = mock(BufferedReader.class);
        int lengthX = 5;

        doReturn(2).when(bufferedReader).read(any(), anyInt(), anyInt());

        char[] inputBody = main.getInputBody(bufferedReader, lengthX);

        assertEquals(5, inputBody.length);
        verify(bufferedReader).read(inputBody, 0, lengthX);
        verify(bufferedReader).read(inputBody, 2, lengthX - 2);
        verify(bufferedReader).read(inputBody, 4, lengthX - 4);
        verifyNoMoreInteractions(bufferedReader);
    }

    @Test
    public void getBodyInput_whenContentLengthEqualsBody() throws IOException {
        Main main = spy(Main.class);
        BufferedReader bufferedReader = mock(BufferedReader.class);
        int lengthX = 6;

        doReturn(2).when(bufferedReader).read(any(), anyInt(), anyInt());

        char[] inputBody = main.getInputBody(bufferedReader, lengthX);

        assertEquals(6, inputBody.length);
        verify(bufferedReader).read(inputBody, 0, lengthX);
        verify(bufferedReader).read(inputBody, 2, lengthX - 2);
        verify(bufferedReader).read(inputBody, 4, lengthX - 4);
        verifyNoMoreInteractions(bufferedReader);
    }

    @Test
    public void getBodyInput_whenContentLengthIsBiggerThanBody() throws IOException {
        Main main = spy(Main.class);
        BufferedReader bufferedReader = mock(BufferedReader.class);
        int lengthX = 7;

        doReturn(2).doReturn(2).doReturn(2).doReturn(-1).when(bufferedReader).read(any(), anyInt(), anyInt());

        char[] inputBody = main.getInputBody(bufferedReader, lengthX);

        assertEquals(7, inputBody.length);
        verify(bufferedReader).read(inputBody, 0, lengthX);
        verify(bufferedReader).read(inputBody, 2, lengthX - 2);
        verify(bufferedReader).read(inputBody, 4, lengthX - 4);
        verify(bufferedReader).read(inputBody, 6, lengthX - 6);
        verifyNoMoreInteractions(bufferedReader);
    }

    @Test
    public void methodControlGET() {
        Request request = spy(Request.class);
        request.inputList = Collections.singletonList("GET /test.txt HTTP/1.1");
        request.parseRequestLine();

        Main main = new Main();
        Response response = main.methodControl(request);

        assertTrue(response instanceof ResponseGET);
    }

    @Test
    public void methodControlHEAD() {
        Request request = spy(Request.class);
        request.inputList = Collections.singletonList("HEAD /test.txt HTTP/1.1");
        request.parseRequestLine();

        Main main = new Main();
        Response response = main.methodControl(request);

        assertTrue(response instanceof ResponseHEAD);
    }

    @Test
    public void methodControlMethodUnknown() {
        Request request = spy(Request.class);
        request.inputList = Collections.singletonList("DELETE /test.txt HTTP/1.1");
        request.parseRequestLine();

        Main main = new Main();
        Response response = main.methodControl(request);

        assertTrue(response instanceof ResponseUnknown);
    }
}